docker context use default
docker stack ls
docker stack rm storage
docker network create storage
docker stack deploy --compose-file ./docker-compose.storage.local.yaml storage