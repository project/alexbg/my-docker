net use Z: \\snas\docker\volumes\elk 
if ERRORLEVEL 1 exit 1

xcopy configs z:\configs\ /ery
if ERRORLEVEL 1 exit 1

mkdir z:\elastic-data
mkdir z:\logs
net use Z: /delete

ssh alexbg@snas chown -R alexbg:docker /volume2/docker/volumes/elk