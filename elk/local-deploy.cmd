call local-docker-volumes.cmd

docker context use default
docker stack ls
docker stack rm elk
timeout 10 > NUL

docker stack deploy --compose-file ./local-docker-compose.yaml elk